with open('INPUT.TXT') as input_file:
    input_data = input_file.read()

maximum = 0
current = 0
for symbol in input_data:
    if symbol == '1':
        current +=1
        if current > maximum:
            maximum = current
    else:
        current = 0

with open("OUTPUT.TXT", 'w') as output_file:
    output_file.write(str(maximum))